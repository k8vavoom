//**************************************************************************
//**
//**    ##   ##    ##    ##   ##   ####     ####   ###     ###
//**    ##   ##  ##  ##  ##   ##  ##  ##   ##  ##  ####   ####
//**     ## ##  ##    ##  ## ##  ##    ## ##    ## ## ## ## ##
//**     ## ##  ########  ## ##  ##    ## ##    ## ##  ###  ##
//**      ###   ##    ##   ###    ##  ##   ##  ##  ##       ##
//**       #    ##    ##    #      ####     ####   ##       ##
//**
//**  Copyright (C) 1999-2006 Jānis Legzdiņš
//**  Copyright (C) 2022 Ketmar Dark
//**
//**  This program is free software: you can redistribute it and/or modify
//**  it under the terms of the GNU General Public License as published by
//**  the Free Software Foundation, version 3 of the License ONLY.
//**
//**  This program is distributed in the hope that it will be useful,
//**  but WITHOUT ANY WARRANTY; without even the implied warranty of
//**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//**  GNU General Public License for more details.
//**
//**  You should have received a copy of the GNU General Public License
//**  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//**
//**************************************************************************
class Cyberdemon : Actor decorate;
class SpiderMastermind : Actor decorate;
class Cacodemon : Actor decorate;
class PainElemental : Actor decorate;


// ////////////////////////////////////////////////////////////////////////// //
class EntityExLudoGibber : replaces(EntityEx, forceparents);


//==========================================================================
//
//  SpawnOneGib
//
//==========================================================================
private Actor SpawnOneGib () {
  if (IRandomBetween(0, 6) > 3) return none;

  Actor A = SpawnEntityChecked(class!Actor, LudoGib_JumpingGib,
                               Origin+vector(FRandomBetween(-Radius, Radius),
                                             FRandomBetween(-Radius, Radius),
                                             FRandomBetween(10, default.Height)));
  if (!A) return none;
  //printdebug("!!!SPAWNED!!!");

  //original:
  //  ZOfs = random(32,10)
  //  XVel = random(3,-3)
  //  YVel = random(3,-3)
  //  ZVel = random(3,1)

  A.Velocity = vector(FRandomBetween(-1.0, 1.0)*3.0*35.0/*+Velocity.x*/,
                      FRandomBetween(-1.0, 1.0)*3.0*35.0/*+Velocity.y*/,
                      ((A.Origin.z-Origin.z)/default.Height+FRandomBetween(0.5, 1.5))*3.0*35.0);
  A.RenderStyle = RenderStyle;
  A.Alpha = Alpha;
  A.CopyFriendliness(self, true);
  // copy translation too, why not
  A.Translation = Translation;

  return A;
}


//==========================================================================
//
//  GibItOut
//
//==========================================================================
private void GibItOut (EntityEx source, EntityEx inflictor) {
  if (/*bIsPlayer ||*/ !bMonster) return; // don't gib a player (why?)
  if (bIceCorpse) return;
  // assume that if there is no blood, the monster doesn't have a meat body
  if (bNoBlood) return;
  // let bosses be gibbed too
  //if (bBoss) return;
  // why? let it gib too, lol
  //if (self isa 'CommanderKeen') return;

  if (!GetCvarB('ludogibs_enabled')) return;

  // extreme death is prohibited?
  if (inflictor && inflictor.bNoExtremeDeath) return; // never

  bool needGib = false;

  if (inflictor && inflictor.bExtremeDeath) {
    needGib = true; // always
  } else if (source && source.bIsPlayer && PlayerEx(source.Player).IsWeaponAlwaysExtremeDeath()) {
    // always extreme death
    needGib = true;
  } else {
    // check for extreme death, the same way the engine does
    int GHealth = GibsHealth;
    if (!GHealth) GHealth = int(float(-InitialHealth)*LineSpecialGameInfo(Level.Game).GibsHealthFactor);
    // just in case; it should always be negative, but...
    if (GHealth > 0) GHealth = -GHealth;
    needGib = (Health < GHealth);
  }


  if (!needGib && !GetCvarB('ludogibs_always')) {
    if ((GetCvarB('ludogibs_always_cybbie') && (self isa Cyberdemon)) ||
        (GetCvarB('ludogibs_always_momma') && (self isa SpiderMastermind)))
    {
      // ok
    } else {
      return;
    }
  }

  //printdebug("!!! %C: GIBBING!", self);
  if (GetCvarB('ludogibs_sound')) PlaySound('Ludogibs/ludogibs', CHAN_AUTO);

  // stop the thing and reset it's height
  /*
  Velocity = vector(0.0, 0.0, 0.0);
  Height = default.Height;
  */

  int eyesleft = (GetCvarB('ludogibs_many_eyes') ? -1 : 2);

  // cacodemon and pain elemental have only one eye ;-)
  if (eyesleft != -1) {
    if ((self isa Cacodemon) || (self isa PainElemental)) {
      eyesleft = 1;
    }
  }

  // calculate number of chunks based on the size of the thing
  int NumChunks = max(4, int(Radius*Height)/32);
  foreach (; 0..max(24, NumChunks+((P_Random()-P_Random())%(NumChunks/4)))) {
    auto gib = LudoGib_JumpingGib(SpawnOneGib());
    if (!gib) continue;
    if (eyesleft > 0) {
      --eyesleft;
      gib.ForceEye();
    } else if (!eyesleft) {
      gib.RandomNoEyes();
    } else {
      gib.RandomAnything();
    }
  }
}


//==========================================================================
//
//  Died
//
//==========================================================================
override void Died (EntityEx source, EntityEx inflictor) {
  //printdebug("*** %C: Died, entering; source=%C; inflictor=%C", self, source, inflictor);
  if (Level.Game.IsAuthority) GibItOut(source, inflictor);
  ::Died(source, inflictor);
  //printdebug("*** %C: Died, exiting; source=%C; inflictor=%C", self, source, inflictor);
}
