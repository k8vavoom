/* coded by Ketmar // Invisible Vector (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
#include <stdarg.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <utime.h>

#include <sys/stat.h>
#include <sys/types.h>

#include "libvwad/vwadvfs.h"


// ////////////////////////////////////////////////////////////////////////// //
#define nullptr  NULL


// ////////////////////////////////////////////////////////////////////////// //
static const uint8_t k8PubKey[32] = {
  0x46,0x0D,0x08,0x92,0x32,0x3A,0xFC,0x41,
  0xDD,0x7D,0x4F,0x5B,0x12,0xCA,0x9E,0x6C,
  0x6F,0x54,0x7D,0x45,0x3F,0xC1,0x5C,0x6A,
  0x80,0xCA,0xED,0x0D,0x17,0x37,0x0F,0xA6,
};


// ////////////////////////////////////////////////////////////////////////// //
static inline __attribute__((cold)) const char *SkipPathPartCStr (const char *s) {
  const char *lastSlash = nullptr;
  for (const char *t = s; *t; ++t) {
    if (*t == '/') lastSlash = t+1;
    #ifdef _WIN32
    if (*t == '\\') lastSlash = t+1;
    #endif
  }
  return (lastSlash ? lastSlash : s);
}

#define vassert(cond_)  do { if (__builtin_expect((!(cond_)), 0)) { fflush(stdout); fflush(stderr); fprintf(stderr, "%s:%d: Assertion in `%s` failed: %s\n", SkipPathPartCStr(__FILE__), __LINE__, __PRETTY_FUNCTION__, #cond_); abort(); } } while (0)


// ////////////////////////////////////////////////////////////////////////// //
static time_t secstart = 0;

static uint64_t sxed_get_msecs (void) {
  struct timespec ts;
  #ifdef CLOCK_MONOTONIC
  //sxed_assert(clock_gettime(CLOCK_MONOTONIC, &ts) == 0);
  clock_gettime(CLOCK_MONOTONIC, &ts);
  #else
  // this should be available everywhere
  //sxed_assert(clock_gettime(CLOCK_REALTIME, &ts) == 0);
  clock_gettime(CLOCK_REALTIME, &ts);
  #endif
  // first run?
  if (secstart == 0) {
    secstart = ts.tv_sec+1;
    //sxed_assert(secstart); // it should not be zero
  }
  return (uint64_t)(ts.tv_sec-secstart+2)*1000U+(uint32_t)ts.tv_nsec/1000000U;
  // nanoseconds
  //return (uint64_t)(ts.tv_sec-secstart+2)*1000000000U+(uint32_t)ts.tv_nsec;
}


// ////////////////////////////////////////////////////////////////////////// //
static int ioseek (vwad_iostream *strm, int pos) {
  vassert(pos >= 0);
  FILE *fl = (FILE *)strm->udata;
  vassert(fl != nullptr);
  if (fseek(fl, pos, SEEK_SET) != 0) return -1;
  return 0;
}

static int ioread (vwad_iostream *strm, void *buf, int bufsize) {
  vassert(bufsize > 0);
  FILE *fl = (FILE *)strm->udata;
  vassert(fl != nullptr);
  if (fread(buf, bufsize, 1, fl) != 1) return -1;
  return 0;
}

static vwad_iostream *new_file_stream (const char *fname) {
  FILE *fl = fopen(fname, "rb");
  if (!fl) {
    fprintf(stderr, "FATAL: cannot open file \"%s\"\n", fname);
    exit(1);
  }
  vwad_iostream *strm = calloc(1, sizeof(vwad_iostream));
  strm->udata = fl;
  strm->seek = ioseek;
  strm->read = ioread;
  return strm;
}


static void close_file_stream (vwad_iostream *strm) {
  if (strm) {
    if (strm->udata) fclose((FILE *)strm->udata);
    memset(strm, 0, sizeof(vwad_iostream));
    free(strm);
  }
}


// ////////////////////////////////////////////////////////////////////////// //
static void logger (int type, const char *fmt, ...) {
  fflush(stdout); fflush(stderr);
  FILE *fo = stdout;
  switch (type) {
    case VWAD_LOG_NOTE: fprintf(fo, "NOTE: "); break;
    case VWAD_LOG_WARNING: fprintf(fo, "WARNING: "); break;
    case VWAD_LOG_ERROR: fprintf(fo, "ERROR: "); break;
    case VWAD_LOG_DEBUG: fo = stderr; fprintf(fo, "DEBUG: "); break;
    default: fprintf(fo, "WUTAFUCK: "); break;
  }
  va_list ap;
  va_start(ap, fmt);
  vfprintf(fo, fmt, ap);
  va_end(ap);
  fputc('\n', fo);
}


static void assertion_failed (const char *fmt, ...) {
  fflush(stderr); fflush(stdout);
  fprintf(stderr, "\nASSERTION FAILED!\n");
  va_list ap;
  va_start(ap, fmt);
  vfprintf(stderr, fmt, ap);
  va_end(ap);
  fputc('\n', stderr);
  exit(1);
}


// ////////////////////////////////////////////////////////////////////////// //
#define COMATOZE_BUF_SIZE  (128)
static char comatoze_buf[COMATOZE_BUF_SIZE];

static const char *comatoze (uint32_t n) {
  char *buf = comatoze_buf;
  int bpos = (int)COMATOZE_BUF_SIZE;
  buf[--bpos] = 0;
  int xcount = 0;
  do {
    if (xcount == 3) { if (bpos > 0) buf[--bpos] = ','; xcount = 0; }
    if (bpos > 0) buf[--bpos] = '0'+n%10;
    ++xcount;
  } while ((n /= 10) != 0);
  return &buf[bpos];
}


// ////////////////////////////////////////////////////////////////////////// //
static vwad_bool debug_chunk_buffering = 0;

static void debug_read_chunk (vwad_handle *wad, int bidx, vwad_fidx fidx, vwad_fd fd, int chunkidx) {
  fprintf(stderr, "READ CHUNK: fd=%d; fidx=%d; bidx=%d; cidx=%d (%s)\n",
          fd, fidx, bidx, chunkidx, vwad_get_file_name(wad, fidx));
}

static void debug_flush_chunk (vwad_handle *wad, int bidx, vwad_fidx fidx, vwad_fd fd, int chunkidx) {
  fprintf(stderr, "FLUSH CHUNK: fd=%d; fidx=%d; bidx=%d; cidx=%d (%s)\n",
          fd, fidx, bidx, chunkidx, vwad_get_file_name(wad, fidx));
}


// ////////////////////////////////////////////////////////////////////////// //
static char *arg_value = NULL;


static int check_arg (int *aidxp, char shortname, const char *longname, int want_value,
                      const int argc, char **argv)
{
  if (arg_value) { free(arg_value); arg_value = NULL; }
  if (*aidxp < 1) *aidxp = 1;
  int aidx = *aidxp;
  if (aidx >= argc) return 0;
  const char *arg = argv[aidx];
  if (!arg || arg[0] != '-') return 0;
  if (arg[1] == '-') {
    if (!longname) return 0;
    arg += 2;
    const size_t lnlen = strlen(longname);
    if (lnlen == 0 || strncmp(arg, longname, lnlen) != 0) return 0;
    if (arg[lnlen] == '=') {
      if (!want_value) {
        fprintf(stderr, "FATAL: option \"--%s\" doesn't need a value!\n", longname);
        exit(1);
      }
      arg_value = strdup(arg + lnlen + 1);
      vassert(arg_value != NULL);
    } else if (!arg[lnlen]) {
      if (want_value) {
        ++aidx;
        if (aidx >= argc || !argv[aidx]) {
          if (want_value > 0) {
            fprintf(stderr, "FATAL: option \"--%s\" requires a value!\n", longname);
            exit(1);
          }
          --aidx;
        } else if (argv[aidx][0] == '-') {
          if (want_value > 0) {
            fprintf(stderr, "FATAL: option \"--%s\" requires a value!\n", longname);
            exit(1);
          } else {
            --aidx;
            arg_value = NULL;
          }
        } else {
          arg_value = strdup(argv[aidx]);
        }
      }
    } else {
      return 0;
    }
  } else {
    if (!shortname || arg[1] != shortname || arg[2]) return 0;
    if (want_value) {
      ++aidx;
      if (aidx >= argc || !argv[aidx]) {
        if (want_value > 0) {
          fprintf(stderr, "FATAL: option \"-%c\" requires a value!\n", shortname);
          exit(1);
        } else {
          --aidx;
        }
      } else if (argv[aidx][0] == '-') {
        if (want_value > 0) {
          fprintf(stderr, "FATAL: option \"--%s\" requires a value!\n", longname);
          exit(1);
        }
        --aidx;
      } else {
        arg_value = strdup(argv[aidx]);
      }
    }
  }
  *aidxp = ++aidx;
  return 1;
}


#define CHECK_ARG(sch_,lnam_)  check_arg(&aidx, (sch_), (lnam_), 0, argc, argv)
#define CHECK_ARG_VAL(sch_,lnam_)  check_arg(&aidx, (sch_), (lnam_), 1, argc, argv)
#define CHECK_ARG_VALX(sch_,lnam_)  check_arg(&aidx, (sch_), (lnam_), -1, argc, argv)


// ////////////////////////////////////////////////////////////////////////// //
typedef struct Mask_t Mask;
struct Mask_t {
  char *mask;
  Mask *next;
};


#define PUT_MASK()  do { \
  if (arg_value != NULL) { \
    Mask *mmx = calloc(1, sizeof(*mmx)); \
    mmx->mask = arg_value; \
    mmx->next = masklist; \
    masklist = mmx; \
    arg_value = NULL; \
  } else { \
    was_no_mask = 1; \
  } \
} while (0)


// ////////////////////////////////////////////////////////////////////////// //
void create_path (const char *path) {
  if (!path || !path[0]) return;
  #if 0
  fprintf(stderr, "***create_path: [%s]\n", path);
  #endif
  char *cpt = calloc(1, strlen(path) + 16);
  size_t dpos = 0;
  if (path[0] == '/') {
    while (path[0] == '/') ++path;
    if (!path[0]) { free(cpt); return; }
    cpt[dpos++] = '/';
  }
  do {
    if (path[0] == '/') {
      vassert(dpos != 0);
      if (cpt[dpos - 1] != '/') {
        cpt[dpos] = 0;
        #if 0
        fprintf(stderr, "  [%s]\n", cpt);
        #endif
        #ifdef _WIN32
        mkdir(cpt);
        #else
        mkdir(cpt, 0755);
        #endif
        cpt[dpos++] = '/';
      }
    } else {
      cpt[dpos++] = path[0];
    }
    ++path;
  } while (path[0]);
  if (dpos != 0 && cpt[dpos - 1] != '/') {
    cpt[dpos] = 0;
    #if 0
    fprintf(stderr, "   [%s]\n", cpt);
    #endif
    #ifdef _WIN32
    mkdir(cpt);
    #else
    mkdir(cpt, 0755);
    #endif
  }
  free(cpt);
}


// ////////////////////////////////////////////////////////////////////////// //
int main (int argc, char **argv) {
  vwad_logf = logger;
  vwad_assertion_failed = assertion_failed;

  Mask *masklist = NULL;
  int was_no_mask = 0;
  char *destdir = NULL;

  #if 1
  char dest[256];
  vassert(vwad_normalize_file_name("./myfile", dest) == VWAD_OK);
  vassert(strcmp(dest, "myfile") == 0);

  vassert(vwad_normalize_file_name("././/.////./myfile", dest) == VWAD_OK);
  vassert(strcmp(dest, "myfile") == 0);

  vassert(vwad_normalize_file_name("/abc/de/fghij//myfile", dest) == VWAD_OK);
  vassert(strcmp(dest, "/abc/de/fghij/myfile") == 0);

  vassert(vwad_normalize_file_name("/abc/de/fghij/../myfile", dest) == VWAD_OK);
  vassert(strcmp(dest, "/abc/de/myfile") == 0);

  vassert(vwad_normalize_file_name("/abc/de/../fghij/../myfile", dest) == VWAD_OK);
  vassert(strcmp(dest, "/abc/myfile") == 0);

  vassert(vwad_normalize_file_name("/abc/de/../../fghij/../myfile", dest) == VWAD_OK);
  vassert(strcmp(dest, "/myfile") == 0);

  vassert(vwad_normalize_file_name("abc/de/../../fghij/../myfile", dest) == VWAD_OK);
  vassert(strcmp(dest, "myfile") == 0);

  vassert(vwad_normalize_file_name("abc/de/../fghij/../myfile", dest) == VWAD_OK);
  vassert(strcmp(dest, "abc/myfile") == 0);

  vassert(vwad_normalize_file_name("/abc/../../fghij/../myfile", dest) != VWAD_OK);

  vassert(vwad_normalize_file_name("abc/de/../fghij/../myfile///./", dest) == VWAD_OK);
  vassert(strcmp(dest, "abc/myfile/") == 0);

  //return 0;
  #endif

  char *infile_name = NULL;
  enum {
    CMD_NONE = 0,
    CMD_TEST,
    CMD_LIST,
    CMD_EXTRACT,
  };
  int cmd = CMD_NONE;
  int inargs = 1;
  int timing = 0;
  unsigned exflags = 0;
  int overwrite = 0;

  int aidx = 1;
  while (aidx < argc) {
    if (inargs && strcmp(argv[aidx], "--") == 0) {
      inargs = 0;
    } else if (inargs && CHECK_ARG('h', "help")) {
      printf(
        "usage: tester infile [options]\n"
        "options:\n"
        "  -i infile  -- input file (--infile)\n"
        "  -d outdir  -- directory to extract (--destdir)\n"
        "  -t [mask]  -- test files (--test)\n"
        "  -l [mask]  -- list files (--list)\n"
        "  -x [mask]  -- extract files (--extract)\n"
        "  -F         -- allow overwrite (--overwrite, --force)\n"
      );
      return 0;
    } else if (inargs && CHECK_ARG_VALX('t', "test")) {
      if (cmd != CMD_NONE && cmd != CMD_TEST) {
        fprintf(stderr, "FATAL: conflicting commands!\n");
        exit(1);
      }
      cmd = CMD_TEST;
      PUT_MASK();
    } else if (inargs && CHECK_ARG_VALX('x', "extract")) {
      if (cmd != CMD_NONE && cmd != CMD_EXTRACT) {
        fprintf(stderr, "FATAL: conflicting commands!\n");
        exit(1);
      }
      cmd = CMD_EXTRACT;
      PUT_MASK();
    } else if (inargs && CHECK_ARG_VALX('l', "list")) {
      if (cmd != CMD_NONE && cmd != CMD_LIST) {
        fprintf(stderr, "FATAL: conflicting commands!\n");
        exit(1);
      }
      cmd = CMD_LIST;
      PUT_MASK();
    } else if (inargs && CHECK_ARG_VAL('d', "destdir")) {
      if (destdir != NULL) {
        fprintf(stderr, "FATAL: duplicate dest dirs!\n");
        exit(1);
      }
      destdir = arg_value; arg_value = NULL;
    } else if (inargs && CHECK_ARG(0, "timing")) {
      timing = 1;
    } else if (inargs && CHECK_ARG(0, "debug-buffering")) {
      debug_chunk_buffering = 1;
    } else if (inargs && CHECK_ARG(0, "noauth")) {
      exflags |= VWAD_OPEN_NO_SIGN_CHECK;
    } else if (inargs && CHECK_ARG(0, "no-auth")) {
      exflags |= VWAD_OPEN_NO_SIGN_CHECK;
    } else if (inargs && CHECK_ARG(0, "nocrc")) {
      exflags |= VWAD_OPEN_NO_CRC_CHECKS;
    } else if (inargs && CHECK_ARG(0, "no-crc")) {
      exflags |= VWAD_OPEN_NO_CRC_CHECKS;
    } else if (inargs && CHECK_ARG('F', "force")) {
      overwrite = 1;
    } else if (inargs && CHECK_ARG('F', "overwrite")) {
      overwrite = 1;
    } else if (inargs && CHECK_ARG_VAL('i', "input")) {
      if (infile_name != NULL) {
        fprintf(stderr, "FATAL: duplicate \"%s\" option!\n", argv[aidx]);
        exit(1);
      }
      infile_name = arg_value; arg_value = NULL;
      if (strcmp(infile_name, "-") == 0) {
        fprintf(stderr, "FATAL: cannot read from stdin (yet)!\n");
        exit(1);
      }
    } else if (inargs && argv[aidx][0] == '-') {
      fprintf(stderr, "FATAL: unknown option \"%s\"!\n", argv[aidx]);
      exit(1);
    } else {
      if (arg_value) free(arg_value);
      arg_value = strdup(argv[aidx]);
      PUT_MASK();
    }
  }

  if (arg_value) { free(arg_value); arg_value = NULL; }

  if (cmd == CMD_NONE) {
    fprintf(stderr, "FATAL: i don't know what to do.\n");
    exit(1);
  }

  if (infile_name == NULL) {
    fprintf(stderr, "FATAL: i don't know what to read.\n");
    exit(1);
  }

  if (masklist == NULL) {
    if (was_no_mask) {
      arg_value = strdup("*");
      PUT_MASK();
    } else {
      fprintf(stderr, "FATAL: to extract everything, use \"*\" mask!\n");
      exit(1);
    }
  }

  if (debug_chunk_buffering) {
    vwad_debug_read_chunk = debug_read_chunk;
    vwad_debug_flush_chunk = debug_flush_chunk;
  }

  vwad_iostream *strm = new_file_stream(infile_name);
  uint64_t stt = sxed_get_msecs();
  vwad_handle *wad = vwad_open_archive(strm, VWAD_OPEN_DEFAULT|exflags, NULL);
  stt = sxed_get_msecs() - stt;
  if (!wad) {
    close_file_stream(strm);
    fprintf(stderr, "FATAL: cannot open VWAD archive \"%s\"!\n", infile_name);
    exit(1);
  }

  if (vwad_has_pubkey(wad) && vwad_is_authenticated(wad)) {
    if (timing) {
      printf("Ed25519 time taken: %d min, %d.%03d sec.\n",
             (int)(stt/1000/60),
             (int)(stt/1000),
             (int)(stt%1000));
    }
    vwad_public_key pubkey;
    if (vwad_get_pubkey(wad, pubkey) == VWAD_OK) {
      if (memcmp(pubkey, k8PubKey, sizeof(pubkey)) == VWAD_OK) {
        printf("CREATOR: Ketmar Dark\n");
      } else {
        printf("CREATOR: <unknown>\n");
      }
    }
  }

  const char *author = vwad_get_archive_author(wad);
  if (author[0]) printf("AUTHOR: %s\n", author);
  const char *title = vwad_get_archive_title(wad);
  if (title[0]) printf("TITLE: %s\n", title);


  const unsigned int csize = vwad_get_archive_comment_size(wad) + 1;
  char *cmt = malloc(csize);
  vwad_get_archive_comment(wad, cmt, csize);
  if (cmt[0]) {
    printf("========\n%s", cmt);
    if (cmt[strlen(cmt) - 1] != '\n') printf("\n");
    printf("========\n");
  }
  free(cmt);

  //vwad_set_archive_cache(wad, 16);

  int fcount = 0;
  vwad_fidx *flist = calloc(vwad_get_archive_file_count(wad) + 1, sizeof(vwad_fidx));

  for (Mask *mask = masklist; mask; mask = mask->next) {
    const char *glob = mask->mask;
    if (!glob || !glob[0]) continue;
    char nname[256];
    if (vwad_normalize_file_name(glob, nname) != VWAD_OK) {
      fprintf(stderr, "WARNING: skipped bad file name: <%s>\n", nname);
    }

    for (vwad_fidx fidx = 0; fidx < vwad_get_archive_file_count(wad); fidx += 1) {
      const char *fname = vwad_get_file_name(wad, fidx);
      if (vwad_wildmatch_path(nname, strlen(nname), fname, strlen(fname)) == VWAD_OK) {
        if (!flist[fidx]) {
          flist[fidx] = 1;
          ++fcount;
        }
        if (!strchr(nname, '?') && !strchr(nname, '*')) {
          char xxfname[256];
          vassert(vwad_normalize_file_name(nname, xxfname) == VWAD_OK);
          vassert(vwad_find_file(wad, xxfname) == fidx);
        }
      } else {
        char xxfname[256];
        if (vwad_normalize_file_name(nname, xxfname) == VWAD_OK) {
          vwad_fidx xfidx = vwad_find_file(wad, xxfname);
          if (xfidx >= 0) {
            vassert(xfidx < vwad_get_archive_file_count(wad));
            if (!flist[xfidx]) {
              flist[xfidx] = 1;
              ++fcount;
            }
          }
        }
      }
    }
  }

  if (fcount == 0) {
    printf("WARNING: no files!\n");
  } else {
    if (destdir == NULL) destdir = strdup("");
    stt = sxed_get_msecs();
    for (vwad_fidx fidx = 0; fidx < vwad_get_archive_file_count(wad); fidx += 1) {
      if (!flist[fidx]) continue;

      const time_t ftime = vwad_get_ftime(wad, fidx);
      struct tm xtm;
      if (ftime) {
        // convert to local time
        xtm = *localtime(&ftime);
      } else {
        memset(&xtm, 0, sizeof(xtm));
      }

      int pksize = 0;
      int ccount = vwad_get_file_chunk_count(wad, fidx);
      for (int cidx = 0; cidx < ccount; ++cidx) {
        int pksz, upksz;
        if (vwad_get_raw_file_chunk_info(wad, fidx, cidx, &pksz, &upksz, NULL) != VWAD_OK) {
          fprintf(stderr, "INTERNAL ERROR IN 'vwad_get_file_chunk_size'!\n");
          exit(1);
        }
        pksize += pksz - 4;
      }

      if (!timing) {
        const int fsz = vwad_get_file_size(wad, fidx);
        fprintf(stdout, " %13s", comatoze(fsz));
        fprintf(stdout, " %13s %3d%%  %04d/%02d/%02d %02d:%02d:%02d  %08X  %s",
                comatoze(pksize),
                (pksize != 0 ? (int)((int64_t)100*pksize/fsz) : 100),
                xtm.tm_year + 1900, xtm.tm_mon + 1, xtm.tm_mday,
                xtm.tm_hour, xtm.tm_min, xtm.tm_sec,
                vwad_get_fcrc32(wad, fidx),
                vwad_get_file_name(wad, fidx));

        const char *gname = vwad_get_file_group_name(wad, fidx);
        if (gname[0]) fprintf(stdout, " -/- [%s]", gname);
      }

      if (cmd == CMD_LIST) {
        // do nothing
        if (!timing) {
          fputc('\n', stdout);
        }
      } else {
        if (!timing) {
          fprintf(stdout, " ... "); fflush(stdout);
        }
        FILE *fo = NULL;
        char *dname = NULL;
        if (cmd == CMD_EXTRACT) {
          dname = calloc(1, strlen(destdir) + strlen(vwad_get_file_name(wad, fidx)) + 64);
          strcpy(dname, destdir);
          size_t dlen = strlen(dname);
          if (dlen != 0 && dname[dlen - 1] != '/') strcat(dname, "/");
          strcat(dname, vwad_get_file_name(wad, fidx));
          if (!overwrite && access(dname, F_OK) == 0) {
            if (!timing) { fputc('\n', stdout); fflush(stdout); }
            fprintf(stderr, "FATAL: cannot overwrite file '%s'!\n", dname);
            exit(1);
          }
          char *slp = strrchr(dname, '/');
          if (slp != NULL && slp != dname) {
            *slp = 0;
            create_path(dname);
            *slp = '/';
          }
          fo = fopen(dname, "wb");
          if (fo == NULL) {
            if (!timing) { fputc('\n', stdout); fflush(stdout); }
            fprintf(stderr, "FATAL: cannot create file \"%s\"!\n", dname);
            exit(1);
          }
        }

        vwad_fd fd = vwad_open_fidx(wad, fidx);
        vassert(fd >= 0);

        uint8_t buf[1021];
        uint32_t crc32 = vwad_crc32_init();
        int total_read = 0;
        int rd = vwad_read(wad, fd, buf, (int)sizeof(buf));
        while (rd > 0) {
          if (fo != NULL) {
            if (fwrite(buf, (uint32_t)rd, 1, fo) != 1) {
              fclose(fo);
              if (!timing) { fputc('\n', stdout); fflush(stdout); }
              fprintf(stderr, "FATAL: cannot write to file \"%s\"!\n", dname);
              exit(1);
            }
          }
          total_read += rd;
          crc32 = vwad_crc32_part(crc32, buf, (uint32_t)rd);
          rd = vwad_read(wad, fd, buf, (int)sizeof(buf));
        }
        vwad_fclose(wad, fd);
        if (fo != NULL) {
          struct utimbuf tbf;
          fclose(fo);
          if (ftime != 0) {
            tbf.actime = (time_t)ftime;
            tbf.modtime = tbf.actime;
            utime(dname, &tbf);
          }
          free(dname);
        }

        if (rd < 0) {
          if (!timing) { fputc('\n', stdout); fflush(stdout); }
          fprintf(stdout, "  ERROR READING AT %d\n", total_read);
        } else {
          crc32 = vwad_crc32_final(crc32);
          if (crc32 != vwad_get_fcrc32(wad, fidx)) {
            if (!timing) { fputc('\n', stdout); fflush(stdout); }
            fprintf(stdout, "FATAL ERROR: CRC32 FAILED for \"%s\": got: 0x%08x, expected: 0x%08x\n",
                    vwad_get_file_name(wad, fidx), crc32, vwad_get_fcrc32(wad, fidx));
            exit(1);
          } else {
            if (!timing) {
              fprintf(stdout, "OK\n");
            }
          }
        }
      }
    }
    stt = sxed_get_msecs() - stt;
    if (timing) {
      printf("time taken: %d min, %d.%03d sec.\n",
             (int)(stt/1000/60),
             (int)(stt/1000),
             (int)(stt%1000));
    }
  }

  free(flist);
  free(destdir);
  free(infile_name);

  while (masklist) {
    Mask *mmx = masklist; masklist = mmx->next;
    free(mmx->mask);
    free(mmx);
  }

  vwad_close_archive(&wad);
  close_file_stream(strm);
  return 0;
}
