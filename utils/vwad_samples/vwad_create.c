/* coded by Ketmar // Invisible Vector (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
#include <dirent.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <strings.h>
#include <time.h>
#include <unistd.h>

#include <sys/stat.h>
#include <sys/types.h>

#include "crypto/prng_randombytes.h"
#include "libvwad/vwadwrite.h"

// not really necessary. it was used to reject
// the archive we are creating right now if it
// accidentally added to the file list. no more.
//#define INODE_PROTECTION


// ////////////////////////////////////////////////////////////////////////// //
static int esc_allowed;
static int comp_level = VWADWR_COMP_BEST;


// ////////////////////////////////////////////////////////////////////////// //
static inline __attribute__((cold)) const char *SkipPathPartCStr (const char *s) {
  const char *lastSlash = NULL;
  for (const char *t = s; *t; ++t) {
    if (*t == '/') lastSlash = t+1;
    #ifdef _WIN32
    if (*t == '\\') lastSlash = t+1;
    #endif
  }
  return (lastSlash ? lastSlash : s);
}

#define vassert(cond_)  do { if (__builtin_expect((!(cond_)), 0)) { fflush(stdout); fflush(stderr); fprintf(stderr, "%s:%d: Assertion in `%s` failed: %s\n", SkipPathPartCStr(__FILE__), __LINE__, __PRETTY_FUNCTION__, #cond_); exit(1); } } while (0)


// ////////////////////////////////////////////////////////////////////////// //
static vwadwr_archive *wad = NULL;
static char *out_filename = NULL;
static FILE *file_out = NULL;
static vwadwr_iostream *outstrm = NULL;
#ifdef INODE_PROTECTION
static ino_t main_file_inode = 0;
#endif
static int success = 0;


// ////////////////////////////////////////////////////////////////////////// //
static void chop_left (char *s) {
  if (s[0]) memmove(s, s + 1, strlen(s)); else s[0] = 0;
}

// malloced
static char *normalize_name (const char *n) {
  if (n == NULL || !n[0]) return strdup("");
  char *res = strdup(n);
  #ifdef WIN32
  char *t;
  for (t = res; *t; ++t) if (*t == '\\') *t = '/';
  #endif
  do {
    if (res[0] == '.') {
      if (!res[1] || res[1] == '/') chop_left(res);
      else { fprintf(stderr, "FATAL: bad name: \"%s\"\n", n); exit(1); }
    } else if (res[0] == '/') {
      chop_left(res);
    }
  } while (res[0] == '.' || res[0] == '/');
  return res;
}


// ////////////////////////////////////////////////////////////////////////// //
static uint64_t total_unpacked = 0;
static uint64_t total_packed = 0;


static inline int total_pack_ratio (void) {
  return (total_unpacked ? (int)((uint64_t)100 * total_packed / total_unpacked) : 100);
}


// ////////////////////////////////////////////////////////////////////////// //
static time_t secstart = 0;

static uint64_t sxed_get_msecs (void) {
  struct timespec ts;
  #ifdef CLOCK_MONOTONIC
  //sxed_assert(clock_gettime(CLOCK_MONOTONIC, &ts) == 0);
  clock_gettime(CLOCK_MONOTONIC, &ts);
  #else
  // this should be available everywhere
  //sxed_assert(clock_gettime(CLOCK_REALTIME, &ts) == 0);
  clock_gettime(CLOCK_REALTIME, &ts);
  #endif
  // first run?
  if (secstart == 0) {
    secstart = ts.tv_sec+1;
    //sxed_assert(secstart); // it should not be zero
  }
  return (uint64_t)(ts.tv_sec-secstart+2)*1000U+(uint32_t)ts.tv_nsec/1000000U;
  // nanoseconds
  //return (uint64_t)(ts.tv_sec-secstart+2)*1000000000U+(uint32_t)ts.tv_nsec;
}


// ////////////////////////////////////////////////////////////////////////// //
static uint64_t lastoutput = 0;
static char xoutbuf[8192];
static int wasxout = 0;
static char pkoutbuf[128];

typedef struct {
  uint64_t stt;
  int fsize;
  int prevcheck;
} PackInfo;

static void pack_progress (PackInfo *nfo, int read, int written) {
  if (esc_allowed && read - nfo->prevcheck >= 1024*1024) {
    nfo->prevcheck = read;
    const uint64_t ctt = sxed_get_msecs();
    if (ctt - nfo->stt > 1400) {
      if (!wasxout) {
        fputs(xoutbuf, stdout);
        wasxout = 1;
        lastoutput = 0;
      }
      nfo->stt = ctt;
      if (pkoutbuf[0]) fputs(pkoutbuf, stdout);
      int doneprc = (int)((uint64_t)100 * (uint32_t)read / (uint32_t)nfo->fsize);
      if (doneprc > 100) doneprc = 100;
      int ratioprc = (int)((uint64_t)100 * (uint32_t)written / (uint32_t)read);
      if (ratioprc > 999) ratioprc = 999;
      snprintf(pkoutbuf, sizeof(pkoutbuf), "[%3d%%] (%3d%%)", doneprc, ratioprc);
      fputs(pkoutbuf, stdout);
      fflush(stdout);
      for (char *t = pkoutbuf; *t; ++t) *t = 0x08;
      lastoutput = ctt;
    }
  }
}


static int pack_file (const char *fname, const char *pkfname) {
  FILE *fl = fopen(fname, "rb");
  if (!fl) {
    fprintf(stderr, "FATAL: cannot open file \"%s\"\n", fname);
    exit(1);
  }

  pkoutbuf[0] = 0;

  uint64_t ftime = 0;
  struct stat st;
  if (fstat(fileno(fl), &st) != 0) {
    fprintf(stderr, "FATAL: cannot stat file \"%s\"\n", fname);
    exit(1);
  }
  if (st.st_size > 0x3fffffff) {
    fprintf(stderr, "FATAL: file \"%s\" is too big\n", fname);
    exit(1);
  }
  ftime = st.st_mtime;

  PackInfo nfo;
  nfo.fsize = (int)st.st_size;
  nfo.prevcheck = 0;
  nfo.stt = sxed_get_msecs();

  vwadwr_fhandle fd = vwadwr_create_file(wad, comp_level,
                                         pkfname, NULL/*group*/, ftime);

  if (fd < 0) {
    fflush(stdout);
    fprintf(stderr, "\nFATAL: cannot create file \"%s\"\n", fname);
    exit(1);
  }

  // write it
  vwadwr_result res;
  char buf[4096];
  for (;;) {
    ssize_t rd = fread(buf, 1, 4096, fl);
    if (rd < 0) {
      fflush(stdout);
      fprintf(stderr, "\nFATAL: cannot read file \"%s\"\n", fname);
      exit(1);
    }
    if (rd == 0) break;
    res = vwadwr_write(wad, fd, buf, (int)rd);
    if (res != VWADWR_OK) {
      fflush(stdout);
      fprintf(stderr, "\nFATAL: cannot write file \"%s\"\n", fname);
      exit(1);
    }
    pack_progress(&nfo, vwadwr_get_file_unpacked_size(wad, fd),
                        vwadwr_get_file_packed_size(wad, fd));
  }

  if (pkoutbuf[0]) {
    fputs(pkoutbuf, stdout);
    fputs("\x1b[K", stdout);
    fflush(stdout);
  }

  // final
  if (vwadwr_flush_file(wad, fd) != VWADWR_OK) {
    fflush(stdout);
    fprintf(stderr, "\nFATAL: cannot write file \"%s\"\n", fname);
    exit(1);
  }

  int upk = vwadwr_get_file_unpacked_size(wad, fd);
  int pk = vwadwr_get_file_packed_size(wad, fd);

  res = vwadwr_close_file(wad, fd);
  if (res != VWADWR_OK) {
    fflush(stdout);
    fprintf(stderr, "\nFATAL: cannot write file \"%s\"\n", fname);
    exit(1);
  }

  fclose(fl);

  total_unpacked += (unsigned)upk;
  total_packed += (unsigned)pk;
  return pk;
}


// ////////////////////////////////////////////////////////////////////////// //
typedef struct FileInfo_t FileInfo;
struct FileInfo_t {
  char *diskname;
  char *wadname;
  uint32_t size;
  FileInfo *next;
};

static FileInfo *fhead = NULL;
static FileInfo *ftail = NULL;
static uint32_t fcount = 0;


// ////////////////////////////////////////////////////////////////////////// //
static void scanDir (const char *path, const char *prefix) {
  DIR *dir = opendir(path);
  if (!dir) return;
  //printf("scanning: %s -> %s\n", path, prefix);
  for (struct dirent *de = readdir(dir); de; de = readdir(dir)) {
    if (de->d_name[0] != '.') {
      //fprintf(stderr, "%s : %s\n", path, de->d_name);
      char *diskname = malloc(strlen(path) + 1 + strlen(de->d_name) + 16);
      sprintf(diskname, "%s/%s", path, de->d_name);
      struct stat st;
      int res = stat(diskname, &st);
      if (res == 0) {
        if (S_ISREG(st.st_mode)
            #ifdef INODE_PROTECTION
            && main_file_inode != st.st_ino
            #endif
           )
        {
          if (st.st_size > 0x3fffffff) {
            fprintf(stderr, "FATAL: file \"%s\" too big\n", diskname);
            exit(1);
          }
          char *wadname = malloc(strlen(prefix) + 1 + strlen(de->d_name) + 16);
          if (prefix && prefix[0]) sprintf(wadname, "%s/%s", prefix, de->d_name);
          else strcpy(wadname, de->d_name);
          FileInfo *fi = malloc(sizeof(FileInfo));
          fi->diskname = diskname;
          fi->wadname = wadname;
          fi->size = (uint32_t)st.st_size;
          fi->next = NULL;
          if (ftail != NULL) ftail->next = fi; else fhead = fi;
          ftail = fi;
          fcount += 1;
          diskname = NULL;
        } else if (S_ISDIR(st.st_mode)) {
          // later
        } else {
          fprintf(stderr, "WTF: %s\n", diskname);
        }
      } else {
        fprintf(stderr, "SHIT: %s\n", diskname);
      }
      if (diskname != NULL) free(diskname);
    }
  }
  closedir(dir);

  dir = opendir(path);
  if (!dir) return;
  for (struct dirent *de = readdir(dir); de; de = readdir(dir)) {
    if (de->d_name[0] != '.') {
      //fprintf(stderr, "%s : %s\n", path, de->d_name);
      char *diskname = malloc(strlen(path) + 1 + strlen(de->d_name) + 16);
      sprintf(diskname, "%s/%s", path, de->d_name);
      struct stat st;
      int res = stat(diskname, &st);
      if (res == 0) {
        if (S_ISDIR(st.st_mode)) {
          char *wadname = malloc(strlen(prefix) + 1 + strlen(de->d_name) + 16);
          if (prefix && prefix[0]) sprintf(wadname, "%s/%s", prefix, de->d_name);
          else strcpy(wadname, de->d_name);
          scanDir(diskname, wadname);
          free(wadname);
        }
      }
      if (diskname != NULL) free(diskname);
    }
  }
  closedir(dir);
}


// ////////////////////////////////////////////////////////////////////////// //
static char xnbuf[64*4];
static int xnbufIdx = 0;


static const char *xnstrip (const char *s) {
  if (!s) s = "";
  size_t slen = strlen(s);
  if (slen < 64) return s;
  xnbufIdx = (xnbufIdx + 1) % 4;
  s += slen - 60;
  if (strlen(s) > 60) abort();
  strcpy(&xnbuf[xnbufIdx * 64], "...");
  strcpy(&xnbuf[xnbufIdx * 64 + 3], s);
  return &xnbuf[xnbufIdx * 64];
}


// ////////////////////////////////////////////////////////////////////////// //
static void packFiles (void) {
  uint32_t fcnt = 0;
  while (fhead != NULL) {
    FileInfo *fi = fhead;
    fhead = fi->next;
    fcnt += 1;
    snprintf(xoutbuf, sizeof(xoutbuf), "%s[%u/%u] %3d%%: packing: %s -> %s ... %s",
            (esc_allowed ? "\r" : ""),
            fcnt, fcount, total_pack_ratio(),
            xnstrip(fi->diskname), xnstrip(fi->wadname),
            (esc_allowed ? "\x1b[K" : ""));
    if (lastoutput == 0 || sxed_get_msecs() - lastoutput >= 333) {
      fputs(xoutbuf, stdout); fflush(stdout);
      wasxout = 1;
      lastoutput = sxed_get_msecs();
    } else {
      wasxout = 0;
    }
    int pks = pack_file(fi->diskname, fi->wadname);
    if (wasxout) {
      if (fi->size) {
        fprintf(stdout, "%d%%", (int)((int64_t)100 * pks / (int64_t)fi->size));
      } else {
        fprintf(stdout, "100%%");
      }
      if (esc_allowed) fflush(stdout); else fputc('\n', stdout);
    }
    free(fi->wadname);
    free(fi->diskname);
    free(fi);
  }
  ftail = NULL;
}


// ////////////////////////////////////////////////////////////////////////// //
/*
static inline uint32_t permuteU32 (uint32_t x) {
  x ^= x>>16;
  x *= 0x7feb352du;
  x ^= x>>15;
  x *= 0x846ca68bu;
  x ^= x>>16;
  return x;
}
*/


// ////////////////////////////////////////////////////////////////////////// //
static void print_key (const char *name, const uint8_t key[32]) {
  vwadwr_z85_key enkey;
  vwadwr_z85_encode_key(key, enkey);
  printf("%s key: %s\n", name, enkey);

  printf("  {");
  for (size_t kidx = 0; kidx < 32; ++kidx) {
    printf("0x%02X,", key[kidx]);
  }
  printf("}\n");
}


// ////////////////////////////////////////////////////////////////////////// //
static void exitfn (void) {
  vwadwr_free_file_stream(outstrm);
  if (!success) {
    if (file_out) fclose(file_out);
    if (out_filename != NULL && out_filename[0]) {
      unlink(out_filename);
    }
  }
}


// ////////////////////////////////////////////////////////////////////////// //
static void logger (int type, const char *fmt, ...) {
  fflush(stdout); fflush(stderr);
  FILE *fo = stdout;
  switch (type) {
    case VWADWR_LOG_NOTE: fprintf(fo, "NOTE: "); break;
    case VWADWR_LOG_WARNING: fprintf(fo, "WARNING: "); break;
    case VWADWR_LOG_ERROR: fprintf(fo, "ERROR: "); break;
    case VWADWR_LOG_DEBUG: fo = stderr; fprintf(fo, "DEBUG: "); break;
    default: fprintf(fo, "WUTAFUCK: "); break;
  }
  va_list ap;
  va_start(ap, fmt);
  vfprintf(fo, fmt, ap);
  va_end(ap);
  fputc('\n', fo);
}


static void assertion_failed (const char *fmt, ...) {
  fflush(stderr); fflush(stdout);
  fprintf(stderr, "\nASSERTION FAILED!\n");
  va_list ap;
  va_start(ap, fmt);
  vfprintf(stderr, fmt, ap);
  va_end(ap);
  fputc('\n', stderr);
  exit(1);
}


// ////////////////////////////////////////////////////////////////////////// //
static char *arg_value = NULL;


static int check_arg (int *aidxp, char shortname, const char *longname, int want_value,
                      const int argc, char **argv)
{
  if (arg_value) { free(arg_value); arg_value = NULL; }
  if (*aidxp < 1) *aidxp = 1;
  int aidx = *aidxp;
  if (aidx >= argc) return 0;
  const char *arg = argv[aidx];
  if (!arg || arg[0] != '-') return 0;
  if (arg[1] == '-') {
    if (!longname) return 0;
    arg += 2;
    const size_t lnlen = strlen(longname);
    if (lnlen == 0 || strncmp(arg, longname, lnlen) != 0) return 0;
    if (arg[lnlen] == '=') {
      if (!want_value) {
        fprintf(stderr, "FATAL: option \"--%s\" doesn't need a value!\n", longname);
        exit(1);
      }
      arg_value = strdup(arg + lnlen + 1);
      vassert(arg_value != NULL);
    } else if (!arg[lnlen]) {
      if (want_value) {
        ++aidx;
        if (aidx >= argc || !argv[aidx]) {
          fprintf(stderr, "FATAL: option \"--%s\" requires a value!\n", longname);
          exit(1);
        }
        arg_value = strdup(argv[aidx]);
      }
    } else {
      return 0;
    }
  } else {
    if (!shortname || arg[1] != shortname || arg[2]) return 0;
      if (want_value) {
      ++aidx;
      if (aidx >= argc || !argv[aidx]) {
        fprintf(stderr, "FATAL: option \"-%c\" requires a value!\n", shortname);
        exit(1);
      }
      arg_value = strdup(argv[aidx]);
    }
  }
  *aidxp = ++aidx;
  return 1;
}


#define CHECK_ARG(sch_,lnam_)  check_arg(&aidx, (sch_), (lnam_), 0, argc, argv)
#define CHECK_ARG_VAL(sch_,lnam_)  check_arg(&aidx, (sch_), (lnam_), 1, argc, argv)


// ////////////////////////////////////////////////////////////////////////// //
int main (int argc, char **argv) {
  vwadwr_logf = logger;
  vwadwr_assertion_failed = assertion_failed;
  esc_allowed = isatty(fileno(stdout));
  char *indir = NULL;
  char *wadpfx = NULL;  // prefix for wad file names
  int has_privkey = 0;
  int print_keys = 0;
  vwadwr_secret_key privkey;
  char *comment = NULL;
  char *author = NULL;
  char *title = NULL;
  vwadwr_bool force_fat = 0;

  int aidx = 1;
  while (aidx < argc) {
    if (CHECK_ARG('h', "help")) {
      printf(
        "usage: creatori [options] srcdir\n"
        "options:\n"
        "  -o outfile\n"
        "  -p pfx          -- directory prefix for packed files\n"
        "  -k <z85key>     -- private key for signing\n"
        "  -k new          -- generate new private key for signing\n"
        "  -k file <fname> -- read private key from <fname> (no file: generate new key and write)\n"
        "  -k print        -- print used private and public keys\n"
        "  -c <fname>      -- read comment from file\n"
        "  -C text         -- comment text\n"
        "  -A author       -- author text\n"
        "  -T title        -- title text\n"
      );
      return 0;
    } else if (CHECK_ARG_VAL('o', "out")) {
      if (out_filename != NULL) {
        fprintf(stderr, "FATAL: duplicate \"-o\"\n");
        return 1;
      }
      if (!arg_value[0]) {
        fprintf(stderr, "FATAL: empty \"-o\" argument\n");
        return 1;
      }
      out_filename = arg_value; arg_value = NULL;
    } else if (CHECK_ARG_VAL('p', "prefix")) {
      if (wadpfx != NULL) {
        fprintf(stderr, "FATAL: duplicate \"-p\"\n");
        return 1;
      }
      if (!arg_value[0]) {
        fprintf(stderr, "FATAL: empty \"-p\" argument\n");
        return 1;
      }
      wadpfx = arg_value; arg_value = NULL;
    } else if (CHECK_ARG_VAL('k', "key")) {
      if (strcmp(arg_value, "print") == 0) {
        print_keys = 1;
        continue;
      }
      if (strcmp(arg_value, "noauth") == 0) {
        if (has_privkey) has_privkey = -1;
        continue;
      }
      if (has_privkey) {
        fprintf(stderr, "FATAL: duplicate \"-k\"\n");
        return 1;
      }
      if (!arg_value[0]) {
        fprintf(stderr, "FATAL: empty \"-k\" command\n");
        return 1;
      }
      if (strcmp(arg_value, "new") == 0) {
        do {
          prng_randombytes(privkey, sizeof(privkey));
        } while (!vwadwr_is_good_privkey(privkey));
        printf("generated new private key.\n");
      } else if (strcmp(arg_value, "file") == 0) {
        if (aidx >= argc || argv[aidx] == NULL || !argv[aidx][0]) {
          fprintf(stderr, "FATAL: empty \"-k\" file name argument\n");
          return 1;
        }
        FILE *kfl = fopen(argv[aidx], "rb");
        if (kfl == NULL) {
          // no file: generate key
          do {
            prng_randombytes(privkey, sizeof(privkey));
          } while (!vwadwr_is_good_privkey(privkey));
          kfl = fopen(argv[aidx], "wb");
          if (kfl == NULL) {
            fprintf(stderr, "FATAL: cannot create private key file \"%s\"\n", argv[aidx]);
            return 1;
          }
          if (fwrite(privkey, sizeof(privkey), 1, kfl) != 1) {
            fprintf(stderr, "FATAL: cannot write private key file \"%s\"\n", argv[aidx]);
            return 1;
          }
          fclose(kfl);
          printf("generated new private key, and written to \"%s\"\n", argv[aidx]);
        } else {
          if (fread(privkey, sizeof(privkey), 1, kfl) != 1) {
            fprintf(stderr, "FATAL: cannot read private key file \"%s\"\n", argv[aidx]);
            return 1;
          }
          fclose(kfl);
          printf("loaded private key.\n");
        }
        ++aidx;
      } else {
        if (vwadwr_z85_decode_key(arg_value, privkey) != 0) {
          fprintf(stderr, "FATAL: cannot decode private key\n");
          return 1;
        }
        printf("decoded private key.\n");
      }
      has_privkey = 1;
    } else if (CHECK_ARG_VAL('c', "comment-file")) {
      if (!arg_value[0]) {
        fprintf(stderr, "FATAL: empty \"-c\" argument\n");
        return 1;
      }
      if (comment != NULL) {
        fprintf(stderr, "FATAL: duplicate \"-c\" option\n");
        return 1;
      }
      FILE *cfl = fopen(arg_value, "rb");
      if (cfl == NULL) {
        fprintf(stderr, "FATAL: cannot open comment file \"%s\"\n", arg_value);
        return 1;
      }
      comment = calloc(1, 65539);
      const ssize_t csz = fread(comment, 1, 65539, cfl);
      fclose(cfl);
      if (csz < 0) {
        fprintf(stderr, "FATAL: cannot read comment file \"%s\"\n", arg_value);
        return 1;
      }
      if (csz > 65535) {
        fprintf(stderr, "FATAL: comment file \"%s\" too big\n", arg_value);
        return 1;
      }
      comment[csz] = 0;
    } else if (CHECK_ARG_VAL('C', "comment")) {
      if (comment != NULL) {
        fprintf(stderr, "FATAL: duplicate \"-c\" option\n");
        return 1;
      }
      const size_t clen = strlen(arg_value);
      if (clen > 65535) {
        fprintf(stderr, "FATAL: comment too big\n");
        return 1;
      }
      comment = arg_value; arg_value = NULL;
    } else if (CHECK_ARG_VAL('A', "author")) {
      if (author != NULL) {
        fprintf(stderr, "FATAL: duplicate \"-A\" option\n");
        return 1;
      }
      const size_t clen = strlen(arg_value);
      if (clen > 127) {
        fprintf(stderr, "FATAL: author too big\n");
        return 1;
      }
      author = arg_value; arg_value = NULL;
    } else if (CHECK_ARG_VAL('T', "title")) {
      if (title != NULL) {
        fprintf(stderr, "FATAL: duplicate \"-T\" option\n");
        return 1;
      }
      const size_t clen = strlen(arg_value);
      if (clen > 127) {
        fprintf(stderr, "FATAL: title too big\n");
        return 1;
      }
      title = arg_value; arg_value = NULL;
    } else if (CHECK_ARG(0, "silly")) {
      comp_level = VWADWR_COMP_FASTEST;
    } else if (CHECK_ARG(0, "fastest")) {
      comp_level = VWADWR_COMP_FASTEST;
    } else if (CHECK_ARG(0, "fast")) {
      comp_level = VWADWR_COMP_FAST;
    } else if (CHECK_ARG(0, "medium")) {
      comp_level = VWADWR_COMP_MEDIUM;
    } else if (CHECK_ARG(0, "max")) {
      comp_level = VWADWR_COMP_BEST;
    } else if (CHECK_ARG(0, "best")) {
      comp_level = VWADWR_COMP_BEST;
    } else if (CHECK_ARG(0, "no-compression")) {
      comp_level = VWADWR_COMP_DISABLE;
    } else if (CHECK_ARG(0, "force-fat")) {
      force_fat = 1;
    } else if (argv[aidx][0] == '-' && argv[aidx][1] == 'z') {
      if (argv[aidx][3]) {
        fprintf(stderr, "FATAL: \"-z\" option without an argument\n");
        return 1;
      }
      if (argv[aidx][2] >= '0' && argv[aidx][2] <= '9') {
        comp_level = argv[aidx][2] - '0';
      } else if (argv[aidx][2] == '-') {
        comp_level = -1;
      } else {
        fprintf(stderr, "FATAL: invalid \"-z\" option argument\n");
        return 1;
      }
      ++aidx;
    } else if (argv[aidx][0] == '-') {
      fprintf(stderr, "FATAL: unknown option \"%s\"\n", argv[aidx]);
      return 1;
    } else {
      if (indir != NULL) {
        fprintf(stderr, "FATAL: duplicate input dir (%s) (%s)\n", argv[aidx], indir);
        return 1;
      }
      indir = strdup(argv[aidx++]);
    }
  }

  if (arg_value) free(arg_value);

  if (indir == NULL) {
    fprintf(stderr, "FATAL: no source dir\n");
    return 1;
  }
  if (out_filename == NULL) {
    fprintf(stderr, "FATAL: no output file\n");
    return 1;
  }

  if (comment && !comment[0]) { free(comment); comment = NULL; }

  wadpfx = normalize_name(wadpfx);

  file_out = fopen(out_filename, "wb+");
  if (file_out == NULL) {
    fprintf(stderr, "FATAL: cannot create output file\n");
    return 1;
  }

  printf("scanning dirs...\n");
  scanDir(indir, wadpfx);
  free(wadpfx);
  free(indir);

  if (fcount == 0) {
    fprintf(stderr, "FATAL: no files found!\n");
    return 1;
  }

  success = 0;
  atexit(exitfn);

  struct stat st;
  int res = stat(out_filename, &st);
  if (res != 0) {
    fprintf(stderr, "FATAL: cannot stat output file\n");
    return 1;
  }
  if (!S_ISREG(st.st_mode)) {
    fprintf(stderr, "FATAL: output file is not a regular file\n");
    return 1;
  }

  #ifdef INODE_PROTECTION
  if (st.st_ino == 0) {
    fprintf(stderr, "FATAL: cannot get main file inode\n");
    return 1;
  }
  main_file_inode = st.st_ino;
  #endif

  outstrm = vwadwr_new_file_stream(file_out);

  vwadwr_public_key pubkey;

  // we always need it
  if (has_privkey == 0) {
    if (!prng_is_strong_seed()) {
      fprintf(stderr, "WARNING! random seed is not stroung enough!\n");
    }
    do {
      prng_randombytes(privkey, sizeof(vwadwr_secret_key));
    } while (!vwadwr_is_good_privkey(privkey));
  } else {
    if (!vwadwr_is_good_privkey(privkey)) {
      fprintf(stderr, "FATAL: given private key is not good\n");
      exit(1);
    }
  }

  int newerr;
  wad = vwadwr_new_archive(NULL, outstrm, author, title, comment,
                           (has_privkey > 0 ? VWADWR_NEW_DEFAULT : VWADWR_NEW_DONT_SIGN),
                           privkey, pubkey, &newerr);
  if (!wad) {
    fprintf(stderr, "FATAL: cannot init header\n");
    exit(1);
  }

  if (force_fat) {
    vwadwr_force_fat(wad);
    if (!vwadwr_is_fat(wad)) abort();
  }

  packFiles();

  if (esc_allowed) {
    fprintf(stdout, "\r\x1b[K");
    fflush(stdout);
  }

  switch (vwadwr_check_dir(wad)) {
    case VWADWR_OK:
      break;
    case VWADWR_ERR_NAMES_ALIGN:
      fprintf(stderr, "FATAL: invalid name table align\n");
      exit(1);
    case VWADWR_ERR_NAMES_SIZE:
      fprintf(stderr, "FATAL: invalid name table size\n");
      exit(1);
    case VWADWR_ERR_CHUNK_COUNT:
      fprintf(stderr, "FATAL: invalid chunk count\n");
      exit(1);
    case VWADWR_ERR_FILE_COUNT:
      fprintf(stderr, "FATAL: invalid file count\n");
      exit(1);
    default:
      fprintf(stderr, "FATAL: invalid something\n");
      exit(1);
  }

  /*
  if (!vwadwr_is_valid_dir(wad)) {
    fprintf(stderr, "FATAL: too many files or chunks\n");
    exit(1);
  }
  */

  if (vwadwr_finish_archive(&wad) != 0) {
    fprintf(stderr, "FATAL: cannot write directory\n");
    exit(1);
  }

  if (has_privkey && print_keys) {
    print_key("private", privkey);
    print_key(" public", pubkey);
  }

  if (fflush(file_out) != 0) {
    fprintf(stderr, "FATAL: cannot flush output file\n");
    exit(1);
  }

  if (fclose(file_out) != 0) {
    file_out = NULL;
    fprintf(stderr, "FATAL: cannot close output file\n");
    exit(1);
  }
  file_out = NULL;
  success = 1;

  fprintf(stdout, "archive created, final ratio: %d%%\n", total_pack_ratio());

  free(out_filename);

  return 0;
}
