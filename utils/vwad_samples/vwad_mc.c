/* coded by Ketmar // Invisible Vector (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
#include <dirent.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <strings.h>
#include <time.h>
#include <unistd.h>

#include <sys/stat.h>
#include <sys/types.h>

#include "crypto/prng_randombytes.h"
#include "libvwad/vwadwrite.h"
#include "libvwad/vwadvfs.h"


// ////////////////////////////////////////////////////////////////////////// //
static inline __attribute__((cold)) const char *SkipPathPartCStr (const char *s) {
  const char *lastSlash = NULL;
  for (const char *t = s; *t; ++t) {
    if (*t == '/') lastSlash = t+1;
    #ifdef _WIN32
    if (*t == '\\') lastSlash = t+1;
    #endif
  }
  return (lastSlash ? lastSlash : s);
}

#define vassert(cond_)  do { if (__builtin_expect((!(cond_)), 0)) { fflush(stdout); fflush(stderr); fprintf(stderr, "%s:%d: Assertion in `%s` failed: %s\n", SkipPathPartCStr(__FILE__), __LINE__, __PRETTY_FUNCTION__, #cond_); exit(1); } } while (0)


// ////////////////////////////////////////////////////////////////////////// //
static vwad_result ioseek (vwad_iostream *strm, int pos) {
  vassert(pos >= 0);
  FILE *fl = (FILE *)strm->udata;
  vassert(fl != NULL);
  if (fseek(fl, pos, SEEK_SET) != 0) return -1;
  return 0;
}

static vwad_result ioread (vwad_iostream *strm, void *buf, int bufsize) {
  vassert(bufsize > 0);
  FILE *fl = (FILE *)strm->udata;
  vassert(fl != NULL);
  if (fread(buf, bufsize, 1, fl) != 1) return -1;
  return 0;
}

static vwad_iostream *vwad_new_file_stream (const char *fname) {
  vwad_iostream *strm;
  FILE *fl = fopen(fname, "rb");
  if (!fl) {
    strm = NULL;
  } else {
    strm = calloc(1, sizeof(vwad_iostream));
    vassert(strm != NULL);
    strm->udata = fl;
    strm->seek = ioseek;
    strm->read = ioread;
  }
  return strm;
}

static void vwad_close_file_stream (vwad_iostream *strm) {
  if (strm) {
    if (strm->udata) fclose((FILE *)strm->udata);
    memset(strm, 0, sizeof(vwad_iostream));
    free(strm);
  }
}


// ////////////////////////////////////////////////////////////////////////// //
/*
AAAAAAA NNN OOOOOOOO GGGGGGGG SSSSSSSS DATETIME [PATH/]FILENAME [-> [PATH/]FILENAME[/]]]

where (things in [] are optional):

AAAAAAA  is the permission string like in ls -l
NNN      is the number of links
OOOOOOOO is the owner (either UID or name)
GGGGGGGG is the group (either GID or name)
SSSSSSSS is the file size
FILENAME is the filename
PATH     is the path from the archive's root without the leading slash (/)
DATETIME has one of the following formats:
      Mon DD hh:mm, Mon DD YYYY, Mon DD YYYY hh:mm, MM-DD-YYYY hh:mm

            where Mon is a three letter English month name, DD is day
            1-31, MM is month 01-12, YYYY is four digit year, hh hour is
            and mm is minute.

If the -> [PATH/]FILENAME part is present, it means:

If permissions start with an l (ell), then it is the name that symlink
points to. (If this PATH starts with a MC vfs prefix, then it is a symlink
somewhere to the other virtual filesystem (if you want to specify path from
the local root, use local:/path_name instead of /path_name, since /path_name
means from root of the archive listed).

If permissions do not start with l, but number of links is greater than one,
then it says that this file should be a hardlinked with the other file.
*/


//==========================================================================
//
//  doList
//
//==========================================================================
void doList (const char *archname, int extended) {
  vwad_iostream *strm = vwad_new_file_stream(archname);
  if (!strm) exit(1);

  vwad_handle *wad = vwad_open_archive(strm, VWAD_OPEN_NO_MAIN_COMMENT|VWAD_OPEN_NO_SIGN_CHECK, NULL);
  if (!wad) exit(1);

  for (vwad_fidx fidx = 0; fidx < vwad_get_archive_file_count(wad); fidx += 1) {
    const char *fname = vwad_get_file_name(wad, fidx);
    time_t ftime = vwad_get_ftime(wad, fidx);
    struct tm xtm;
    if (ftime) {
      // convert to local time
      xtm = *localtime(&ftime);
    } else {
      memset(&xtm, 0, sizeof(xtm));
    }

    if (extended) printf("[vwad] "); //???
    printf("-rw-r--r--    1 1000     100      %8d %02d-%02d-%04d %02d:%02d:%02d %s\n",
           vwad_get_file_size(wad, fidx),
           xtm.tm_mon + 1, xtm.tm_mday, xtm.tm_year + 1900, xtm.tm_hour, xtm.tm_min, xtm.tm_sec,
           fname);
  }

  vwad_close_archive(&wad);
  vwad_close_file_stream(strm);
}


//==========================================================================
//
//  doExtract
//
//==========================================================================
void doExtract (const char *archname, const char *pkname, const char *destname) {
  char xname[256];
  if (vwad_normalize_file_name(pkname, xname) != 0) exit(1);

  vwad_iostream *strm = vwad_new_file_stream(archname);
  if (!strm) exit(1);

  vwad_handle *wad = vwad_open_archive(strm, VWAD_OPEN_NO_MAIN_COMMENT|VWAD_OPEN_NO_SIGN_CHECK, NULL);
  if (!wad) exit(1);

  vwad_fidx fidx = vwad_find_file(wad, xname);
  if (fidx < 0) {
    vwad_close_archive(&wad);
    vwad_close_file_stream(strm);
    exit(1);
  }

  FILE *fo = fopen(destname, "wb");
  if (fo == NULL) {
    vwad_close_archive(&wad);
    vwad_close_file_stream(strm);
    exit(1);
  }

  vwad_fd fd = vwad_open_fidx(wad, fidx);
  vassert(fd >= 0);

  char buf[1024];
  for (;;) {
    int rd = vwad_read(wad, fd, buf, (int)sizeof(buf));
    if (rd < 0) {
      fclose(fo);
      vwad_close_archive(&wad);
      vwad_close_file_stream(strm);
      exit(1);
    }
    if (rd == 0) break;
    if (fwrite(buf, (unsigned)rd, 1, fo) != 1) {
      fclose(fo);
      vwad_close_archive(&wad);
      vwad_close_file_stream(strm);
      exit(1);
    }
  }

  fclose(fo);

  vwad_close_archive(&wad);
  vwad_close_file_stream(strm);
}


//==========================================================================
//
//  packFile
//
//  return 0 on success
//
//==========================================================================
int packFile (vwadwr_archive *wad, FILE *filein, const char *fname, uint64_t ftime) {
  vwadwr_fhandle fd = vwadwr_create_file(wad, VWADWR_COMP_BEST,
                                         fname, NULL/*group*/, ftime);

  if (fd < 0) {
    fprintf(stderr, "FATAL: cannot write\n");
    return -1;
  }

  // write it
  vwadwr_result res;
  char buf[4096];
  for (;;) {
    ssize_t rd = fread(buf, 1, 4096, filein);
    if (rd < 0) {
      fprintf(stderr, "FATAL: cannot read\n");
      return -1;
    }
    if (rd == 0) break;
    res = vwadwr_write(wad, fd, buf, (int)rd);
    if (res != VWADWR_OK) {
      fprintf(stderr, "FATAL: cannot write\n");
      return -1;
    }
  }

  res = vwadwr_close_file(wad, fd);
  if (res != VWADWR_OK) {
    fprintf(stderr, "FATAL: cannot write\n");
    return -1;
  }

  return 0;
}


//==========================================================================
//
//  copyFile
//
//  return 0 on success
//
//==========================================================================
int copyFile (vwadwr_archive *wad, vwad_handle *src, vwad_fidx fidx) {
  vwadwr_result res;
  vwadwr_fhandle fd = vwadwr_create_raw_file(wad,
                                             vwad_get_file_name(src, fidx),
                                             vwad_get_file_group_name(src, fidx),
                                             vwad_get_fcrc32(src, fidx),
                                             vwad_get_ftime(src, fidx));
  if (fd < 0) {
    fprintf(stderr, "FATAL: cannot write\n");
    return -1;
  }

  const int cleft = vwad_get_file_chunk_count(src, fidx);
  if (cleft < 0) {
    fprintf(stderr, "FATAL: cannot read\n");
    return -1;
  }

  if (cleft > 0) {
    // write it
    char *buf = malloc(VWAD_MAX_RAW_CHUNK_SIZE);
    int pksz, upksz;
    vwad_bool packed;
    int fchunk = 0;
    while (fchunk != cleft) {
      res = vwad_get_raw_file_chunk_info(src, fidx, fchunk, &pksz, &upksz, &packed);
      if (res != VWAD_OK) {
        free(buf);
        fprintf(stderr, "FATAL: cannot read\n");
        return -1;
      }
      res = vwad_read_raw_file_chunk(src, fidx, fchunk, buf);
      if (res != VWAD_OK) {
        free(buf);
        fprintf(stderr, "FATAL: cannot read\n");
        return -1;
      }
      res = vwadwr_write_raw_chunk(wad, fd, buf, pksz, upksz, packed);
      if (res != VWADWR_OK) {
        free(buf);
        fprintf(stderr, "FATAL: cannot write\n");
        return -1;
      }
      fchunk += 1;
    }
    free(buf);
  }

  res = vwadwr_close_file(wad, fd);
  if (res != VWADWR_OK) {
    fprintf(stderr, "FATAL: cannot write\n");
    return -1;
  }

  return 0;
}


//==========================================================================
//
//  doAppend
//
//  also, replacement
//
//==========================================================================
void doAppend (const char *archname, const char *pkname, const char *srcname) {
  //#define DEBUG_DO_APPEND
  char xname[256];
  if (vwad_normalize_file_name(pkname, xname) != 0) exit(1);
  if (xname[0] == '/') memmove(xname, xname + 1, strlen(xname));
  if (!vwadwr_is_valid_file_name(xname)) {
    fprintf(stderr, "FATAL: invalid dest file name: '%s'!\n", pkname);
    exit(1);
  }

  vwad_iostream *strm = vwad_new_file_stream(archname);
  if (!strm) exit(1);

  vwad_handle *wad = vwad_open_archive(strm, VWAD_OPEN_NO_SIGN_CHECK, NULL);
  if (!wad) exit(1);

  // if we have a digital signature, consider the archive read-only
  if (vwad_has_pubkey(wad)) {
    fprintf(stderr, "FATAL: cannot modify signed archives!\n");
    exit(1);
  }

  FILE *filein = fopen(srcname, "rb");
  if (filein == NULL) {
    fprintf(stderr, "FATAL: cannot open input file!\n");
    exit(1);
  }

  uint64_t inftime = 0;
  struct stat st;
  if (fstat(fileno(filein), &st) == 0) {
    inftime = st.st_mtime;
  }

  {
    uint32_t rnd;
    char *newname = malloc(strlen(archname) + 64);
    prng_randombytes(&rnd, sizeof(rnd));
    sprintf(newname, "%s.$$$_%u", archname, rnd);

    #ifdef DEBUG_DO_APPEND
    fprintf(stderr, "temp archive: [%s]\n", newname);
    #endif

    FILE *fout = fopen(newname, "wb+");
    if (fout == NULL) {
      fprintf(stderr, "FATAL: cannot create temp archive!\n");
      exit(1);
    }

    vwadwr_iostream *outstrm = vwadwr_new_file_stream(fout);
    if (outstrm == NULL) {
      fclose(fout);
      unlink(newname);
      fprintf(stderr, "FATAL: cannot create temp archive stream!\n");
      exit(1);
    }

    char *comment = malloc(65536);
    vwad_get_archive_comment(wad, comment, 65536);

    vwadwr_secret_key privkey;
    do {
      prng_randombytes(privkey, sizeof(vwadwr_secret_key));
    } while (!vwadwr_is_good_privkey(privkey));

    vwadwr_archive *newwad = vwadwr_new_archive(NULL, outstrm,
                                                vwad_get_archive_author(wad),
                                                vwad_get_archive_title(wad),
                                                comment,
                                                VWADWR_NEW_DONT_SIGN,
                                                privkey, NULL, NULL);
    free(comment);
    if (newwad == NULL) {
      fclose(fout);
      unlink(newname);
      fprintf(stderr, "FATAL: cannot init new archive!\n");
      exit(1);
    }

    vwad_bool copied = 0;
    for (vwad_fidx fidx = 0; fidx < vwad_get_archive_file_count(wad); fidx += 1) {
      if (!copied && vwad_str_equ_ci(xname, vwad_get_file_name(wad, fidx))) {
        copied = 1;
        if (packFile(newwad, filein, xname, inftime) != 0) {
          fclose(filein);
          fclose(fout);
          unlink(newname);
          fprintf(stderr, "FATAL: cannot append file!\n");
          exit(1);
        }
      } else {
        // copy this file
        vwadwr_result crr = copyFile(newwad, wad, fidx);
        #ifdef DEBUG_DO_APPEND
        const char *fname = vwad_get_file_name(wad, fidx);
        fprintf(stderr, "COPIED: [%s] (%d)\n", fname, crr);
        #endif
        if (crr != 0) {
          fclose(fout);
          unlink(newname);
          fprintf(stderr, "FATAL: cannot copy file!\n");
          exit(1);
        }
      }
    }

    if (!copied) {
      if (packFile(newwad, filein, xname, inftime) != 0) {
        fclose(filein);
        fclose(fout);
        unlink(newname);
        fprintf(stderr, "FATAL: cannot append file!\n");
        exit(1);
      }
    }

    if (vwadwr_finish_archive(&newwad) != 0) {
      fclose(fout);
      unlink(newname);
      fprintf(stderr, "FATAL: cannot finish temp archive!\n");
      exit(1);
    }

    vwadwr_free_file_stream(outstrm);
    fclose(fout);

    vwad_close_archive(&wad);
    vwad_close_file_stream(strm);

    if (rename(newname, archname) != 0) {
      fprintf(stderr, "FATAL: cannot rename temp archive!\n");
      unlink(newname);
      exit(1);
    }

    free(newname);
  }
}


//==========================================================================
//
//  doRemove
//
//==========================================================================
void doRemove (const char *archname, const char *pkname) {
  //#define DEBUG_DO_REMOVE
  char xname[256];
  if (vwad_normalize_file_name(pkname, xname) != 0) exit(1);
  if (xname[0] == '/') memmove(xname, xname + 1, strlen(xname));

  vwad_iostream *strm = vwad_new_file_stream(archname);
  if (!strm) exit(1);

  vwad_handle *wad = vwad_open_archive(strm, VWAD_OPEN_NO_SIGN_CHECK, NULL);
  if (!wad) exit(1);

  // if we have a digital signature, consider the archive read-only
  if (vwad_has_pubkey(wad)) {
    fprintf(stderr, "FATAL: cannot modify signed archives!\n");
    exit(1);
  }

  int haswk = 0;

  for (vwad_fidx fidx = 0; fidx < vwad_get_archive_file_count(wad); fidx += 1) {
    const char *fname = vwad_get_file_name(wad, fidx);
    if (vwad_str_equ_ci(xname, fname)) {
      #ifdef DEBUG_DO_REMOVE
      fprintf(stderr, "FOUND: [%s]\n", fname);
      #endif
      haswk = 1;
      break;
    }
  }

  if (haswk) {
    uint32_t rnd;
    char *newname = malloc(strlen(archname) + 64);
    prng_randombytes(&rnd, sizeof(rnd));
    sprintf(newname, "%s.$$$_%u", archname, rnd);

    #ifdef DEBUG_DO_REMOVE
    fprintf(stderr, "temp archive: [%s]\n", newname);
    #endif

    FILE *fout = fopen(newname, "wb+");
    if (fout == NULL) {
      fprintf(stderr, "FATAL: cannot create temp archive!\n");
      exit(1);
    }

    vwadwr_iostream *outstrm = vwadwr_new_file_stream(fout);
    if (outstrm == NULL) {
      fclose(fout);
      unlink(newname);
      fprintf(stderr, "FATAL: cannot create temp archive stream!\n");
      exit(1);
    }

    char *comment = malloc(65536);
    vwad_get_archive_comment(wad, comment, 65536);

    vwadwr_secret_key privkey;
    do {
      prng_randombytes(privkey, sizeof(vwadwr_secret_key));
    } while (!vwadwr_is_good_privkey(privkey));

    vwadwr_archive *newwad = vwadwr_new_archive(NULL, outstrm,
                                                vwad_get_archive_author(wad),
                                                vwad_get_archive_title(wad),
                                                comment,
                                                VWADWR_NEW_DONT_SIGN,
                                                privkey, NULL, NULL);
    free(comment);
    if (newwad == NULL) {
      fclose(fout);
      unlink(newname);
      fprintf(stderr, "FATAL: cannot init new archive!\n");
      exit(1);
    }

    for (vwad_fidx fidx = 0; fidx < vwad_get_archive_file_count(wad); fidx += 1) {
      const char *fname = vwad_get_file_name(wad, fidx);
      if (vwad_str_equ_ci(xname, fname)) {
        // skip this file
        #ifdef DEBUG_DO_REMOVE
        fprintf(stderr, "SKIP: [%s]\n", fname);
        #endif
      } else {
        // copy this file
        vwadwr_result crr = copyFile(newwad, wad, fidx);
        #ifdef DEBUG_DO_REMOVE
        fprintf(stderr, "COPIED: [%s] (%d)\n", fname, crr);
        #endif
        if (crr != 0) {
          fclose(fout);
          unlink(newname);
          fprintf(stderr, "FATAL: cannot copy file!\n");
          exit(1);
        }
      }
    }

    if (vwadwr_finish_archive(&newwad) != 0) {
      fclose(fout);
      unlink(newname);
      fprintf(stderr, "FATAL: cannot finish temp archive!\n");
      exit(1);
    }

    vwadwr_free_file_stream(outstrm);
    fclose(fout);

    vwad_close_archive(&wad);
    vwad_close_file_stream(strm);

    if (rename(newname, archname) != 0) {
      fprintf(stderr, "FATAL: cannot rename temp archive!\n");
      unlink(newname);
      exit(1);
    }

    free(newname);
  } else {
    vwad_close_archive(&wad);
    vwad_close_file_stream(strm);
  }
}


//==========================================================================
//
//  cmp_path_prefix
//
//==========================================================================
static vwad_bool cmp_path_prefix (const char *fname, const char *pfx) {
  if (!fname || !pfx) return 0;
  uint16_t c0 = vwad_uni_tolower(vwad_utf_decode(&fname));
  uint16_t c1 = vwad_uni_tolower(vwad_utf_decode(&pfx));
  while (c0 && c1 && c0 == c1) {
    if (c0 == VWAD_REPLACEMENT_CHAR || c1 == VWAD_REPLACEMENT_CHAR) return 0;
    c0 = vwad_uni_tolower(vwad_utf_decode(&fname));
    c1 = vwad_uni_tolower(vwad_utf_decode(&pfx));
  }
  if (c1 != 0) return 0;
  if (c0 == 0 || c0 == '/') return 1;
  return 0;
}


//==========================================================================
//
//  doRemoveDir
//
//  this actually used only to remove empty dirs. alas.
//
//==========================================================================
void doRemoveDir (const char *archname, const char *pkname) {
  //#define DEBUG_DO_REMOVE_DIR
  char xname[256];
  if (vwad_normalize_file_name(pkname, xname) != 0) exit(1);
  if (xname[0] == '/') memmove(xname, xname + 1, strlen(xname));

  vwad_iostream *strm = vwad_new_file_stream(archname);
  if (!strm) exit(1);

  vwad_handle *wad = vwad_open_archive(strm, VWAD_OPEN_NO_SIGN_CHECK, NULL);
  if (!wad) exit(1);

  // if we have a digital signature, consider the archive read-only
  if (vwad_has_pubkey(wad)) {
    fprintf(stderr, "FATAL: cannot modify signed archives!\n");
    exit(1);
  }

  int haswk = 0;

  for (vwad_fidx fidx = 0; fidx < vwad_get_archive_file_count(wad); fidx += 1) {
    const char *fname = vwad_get_file_name(wad, fidx);
    if (cmp_path_prefix(fname, xname)) {
      #ifdef DEBUG_DO_REMOVE_DIR
      fprintf(stderr, "FOUND: [%s]\n", fname);
      #endif
      haswk = 1;
      break;
    }
  }

  if (haswk) {
    uint32_t rnd;
    char *newname = malloc(strlen(archname) + 64);
    prng_randombytes(&rnd, sizeof(rnd));
    sprintf(newname, "%s.$$$_%u", archname, rnd);

    #ifdef DEBUG_DO_REMOVE_DIR
    fprintf(stderr, "temp archive: [%s]\n", newname);
    #endif

    FILE *fout = fopen(newname, "wb+");
    if (fout == NULL) {
      fprintf(stderr, "FATAL: cannot create temp archive!\n");
      exit(1);
    }

    vwadwr_iostream *outstrm = vwadwr_new_file_stream(fout);
    if (outstrm == NULL) {
      fclose(fout);
      unlink(newname);
      fprintf(stderr, "FATAL: cannot create temp archive stream!\n");
      exit(1);
    }

    char *comment = malloc(65536);
    vwad_get_archive_comment(wad, comment, 65536);

    vwadwr_secret_key privkey;
    do {
      prng_randombytes(privkey, sizeof(vwadwr_secret_key));
    } while (!vwadwr_is_good_privkey(privkey));

    vwadwr_archive *newwad = vwadwr_new_archive(NULL, outstrm,
                                                vwad_get_archive_author(wad),
                                                vwad_get_archive_title(wad),
                                                comment,
                                                VWADWR_NEW_DONT_SIGN,
                                                privkey, NULL, NULL);
    free(comment);
    if (newwad == NULL) {
      fclose(fout);
      unlink(newname);
      fprintf(stderr, "FATAL: cannot init new archive!\n");
      exit(1);
    }


    for (vwad_fidx fidx = 0; fidx < vwad_get_archive_file_count(wad); fidx += 1) {
      const char *fname = vwad_get_file_name(wad, fidx);
      if (cmp_path_prefix(fname, xname)) {
        // skip this file
        #ifdef DEBUG_DO_REMOVE_DIR
        fprintf(stderr, "SKIP: [%s]\n", fname);
        #endif
      } else {
        // copy this file
        vwadwr_result crr = copyFile(newwad, wad, fidx);
        #ifdef DEBUG_DO_REMOVE_DIR
        fprintf(stderr, "COPIED: [%s] (%d)\n", fname, crr);
        #endif
        if (crr != 0) {
          fclose(fout);
          unlink(newname);
          fprintf(stderr, "FATAL: cannot copy file!\n");
          exit(1);
        }
      }
    }

    if (vwadwr_finish_archive(&newwad) != 0) {
      fclose(fout);
      unlink(newname);
      fprintf(stderr, "FATAL: cannot finish temp archive!\n");
      exit(1);
    }

    vwadwr_free_file_stream(outstrm);
    fclose(fout);

    vwad_close_archive(&wad);
    vwad_close_file_stream(strm);

    if (rename(newname, archname) != 0) {
      fprintf(stderr, "FATAL: cannot rename temp archive!\n");
      unlink(newname);
      exit(1);
    }

    free(newname);
  } else {
    vwad_close_archive(&wad);
    vwad_close_file_stream(strm);
  }
}


// ////////////////////////////////////////////////////////////////////////// //
static __attribute__((unused)) void logger (int type, const char *fmt, ...) {
  fflush(stdout); fflush(stderr);
  FILE *fo = stdout;
  switch (type) {
    case VWAD_LOG_NOTE: fprintf(fo, "NOTE: "); break;
    case VWAD_LOG_WARNING: fprintf(fo, "WARNING: "); break;
    case VWAD_LOG_ERROR: fprintf(fo, "ERROR: "); break;
    case VWAD_LOG_DEBUG: fo = stderr; fprintf(fo, "DEBUG: "); break;
    default: fprintf(fo, "WUTAFUCK: "); break;
  }
  va_list ap;
  va_start(ap, fmt);
  vfprintf(fo, fmt, ap);
  va_end(ap);
  fputc('\n', fo);
}


static __attribute__((unused)) void assertion_failed (const char *fmt, ...) {
  fflush(stderr); fflush(stdout);
  fprintf(stderr, "\nASSERTION FAILED!\n");
  va_list ap;
  va_start(ap, fmt);
  vfprintf(stderr, fmt, ap);
  va_end(ap);
  fputc('\n', stderr);
  exit(1);
}


// ////////////////////////////////////////////////////////////////////////// //
int main (int argc, char **argv) {
  if (argc < 3) {
    fprintf(stderr, "wtf?!\n");
    return 1;
  }

  #if 0
  vwad_logf = logger;
  #endif
  vwad_assertion_failed = assertion_failed;

  const char *cmd = argv[1];

  if (strcmp(cmd, "list") == 0) {
    // list archivename
    if (argc != 3) {
      fprintf(stderr, "wtf?!\n");
      return 1;
    }
    doList(argv[2], 0);
  } else if (strcmp(cmd, "list_ex") == 0) {
    // list_ex archivename
    if (argc != 3) {
      fprintf(stderr, "wtf?!\n");
      return 1;
    }
    doList(argv[2], 1);
  } else if (strcmp(cmd, "copyout") == 0) {
    // copyout archivename storedfilename extractto
    if (argc != 5) {
      fprintf(stderr, "wtf?!\n");
      return 1;
    }
    doExtract(argv[2], argv[3], argv[4]);
  } else if (strcmp(cmd, "copyin") == 0) {
    // copyin archivename storedfilename sourcefile
    // this also does replacement
    doAppend(argv[2], argv[3], argv[4]);
  } else if (strcmp(cmd, "rm") == 0) {
    // rm archivename storedfilename
    doRemove(argv[2], argv[3]);
  } else if (strcmp(cmd, "mkdir") == 0) {
    // mkdir archivename dirname
    return -1;
  } else if (strcmp(cmd, "rmdir") == 0) {
    // rmdir archivename dirname
    doRemoveDir(argv[2], argv[3]);
  } else if (strcmp(cmd, "run") == 0) {
    // ???
    return -1;
  }

  return 0;
}
